import numpy as np
import george
from george import kernels


class Oracle(object):
    def __init__(self, model='Normal'):
        if model == 'Normal':
            self.build_normal_model()

    def build_normal_model(self):
        freq = np.array([0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0])
        self.intensity = np.array([0.3, 0.2, 0.15, 0.12, 0.13, 0.18, 0.19, 0.21, 0.16, 0.2, 0.3])
        intensity_err = 0.1 * np.ones_like(freq)
        kernel = kernels.ExpSquaredKernel(0.1)
        self.gp = george.GP(kernel)
        self.gp.compute(freq, intensity_err)

    def query(self, x_pred):
        pred, _ = self.gp.predict(self.intensity, x_pred[0], return_var=False)
        if x_pred[1] > pred:
            return True

        return False

