from learners import ActiveLearner
from query_strategies import BALD

learner = ActiveLearner(
    query_strategy=BALD,
    budget=10,
    base_kernels=["PER", "LIN", "SE", "LG"],
    max_depth=2,
    ndim=2,
)

# print(learner)


# # =============================================================================
# from data import VectorData

# vd = VectorData()
# if vd:
#     print("VD is not empty")
# else:
#     print("VD is empty")

# vd.update([1, 2], 1)
# vd.update([3, 2], 1.1)
# vd.update([4, 5], 2.2)
# vd.update([3, 5], 3.3)

# if vd:
#     print("VD is not empty")
# else:
#     print("VD is empty")

# print("Length of data: {}".format(len(vd)))
# print(vd)
# vd.plot()

# # =============================================================================
# import numpy as np
# from query_strategies import HyperCubePool

# hcp = HyperCubePool(2, 128)
# print(len(hcp))
# print(hcp[np.random.randint(128)])


from oracle import Oracle

orc = Oracle()
print(orc.query([0.5, 0.5]))
print(orc.query([0.3, 0.1]))
print(orc.query([0.7, 0.25]))



learner.learn(orc.query, verbose=True)


# while learner.budget > 0:
#   x = learner.next_query()
#   y = learner.query(oracle, x)
#   learner.update(x, y)